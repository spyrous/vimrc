"Change buffers with space J and space K
map <Leader>k :bn<CR>
map <Leader>j :bp<CR>

function! CycleReg()
    "make it trigger only on yank and not on delete
    if v:event.operator == 'y'
        let @9 = @8
        let @8 = @7
        let @7 = @6
        let @6 = @5
        let @5 = @4
        let @4 = @3
        let @3 = @2
        let @2 = @1
        let @1 = @0
    endif
endfunction
autocmd TextYankPost * call CycleReg()
"terminal close 
"tnoremap <silent> <C-[><C-[> <C-\><C-n>
tnoremap <C-n> <C-\><C-n>
tnoremap <Esc> <C-\><C-n>
"
" to type the litteral character enter type ctrl-r and then CR
" :digraphs for more
let @c = '/Loaded handDj0'
"Shortcut to change worl under cursor
":nnoremap <Leader>s :%s/\<<C-r><C-w>\>/


"extremely helpfull
":help key-notation
"
"open on last opened
"nvim -c ":execute \"normal! \<C-o>\""
"abbrevation
iab frida log(Thread.backtrace(this.context, Backtracer.ACCURATE).map(DebugSymbol.fromAddress).join('\n'));<ESC>F(3;
