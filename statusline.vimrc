"-- "┃", "█", "", "", "", "", "", "", "●"
function! Vis()
    if mode() ==# "\<C-V>"
        return 1
    else 
        return 0
    endif
endfunction
set laststatus=2
set statusline=
set statusline+=%#BlueBack#%{(mode()==#'n')?'\ \ NORMAL\ \ ':''}
set statusline+=%#GreyBack#%{(mode()==#'i')?'\ \ INSERT\ \ ':''}
set statusline+=%#RedBack#%{(mode()==#'R')?'\ \ REPLACE\ ':''}
set statusline+=%#RedBack#%{(mode()==#'c')?'\ \ COMMAND\ ':''}
set statusline+=%#RedBack#%{(mode()==#'!')?'\ \ EXTCMD\ ':''}
set statusline+=%#GreenBack#%{(mode()==#'v')?'\ \ VISUAL\ \ ':''}
set statusline+=%{(mode()==#'V')?'\ \ \ L-VIS\ \ ':''}
set statusline+=%{(Vis()==1)?'\ \ \ B-VIS\ \ ':''}
set statusline+=%{(mode()==#'t')?'\ \ Ctrl+N\ \ ':''}
set statusline+=%#BlueGrey#%{(mode()==#'n')?'\ ':''}
set statusline+=%#GreyGrey#%{(mode()==#'i')?'\ ':''}
set statusline+=%#RedGrey#%{(mode()==#'R')?'\ ':''}
set statusline+=%#RedGrey#%{(mode()==#'c')?'\ ':''}
set statusline+=%#RedGrey#%{(mode()==#'!')?'\ ':''}
set statusline+=%#GreenGrey#%{(mode()==#'v')?'\ ':''}
set statusline+=%#GreenGrey#%{(mode()==#'V')?'\ ':''}
set statusline+=%#GreenGrey#%{(Vis()==1)?'\ ':''}
set statusline+=%#GreenGrey#%{(mode()==#'t')?'\ ':''}
set statusline+=%0* 
set statusline+=%#User1Inverse#\ \ 
set statusline+=%#User1Inverse#\ %F
set statusline+=%= "changes alignment to right
set statusline+=
set statusline+=%1*\ 
set statusline+=%1*
set statusline+=%1*%{b:gitbranch}
set statusline+=%P
set statusline+=\ 
set statusline+=|
set statusline+=\ 
set statusline+=%#UnderLine#%c%1*
set statusline+=/
set statusline+=%l
set statusline+=:
set statusline+=%L
"iceberg colors
hi BlueGrey guifg=#757ca3 guibg=#adadad
hi GreenGrey guifg=#b6bd89 guibg=#adadad
hi GreenBack guibg=#b6bd89 guifg=#e8e9ec
hi RedGrey guifg=#d9a67f guibg=#adadad
hi UnderLine ctermfg=007 ctermbg=239 guibg=#4e4e4e guifg=#adadad gui=underline

hi BlueBack guibg=#757ca3
hi RedBack guibg=#d9a67f guifg=#e8e9ec
hi GreyGrey guifg=#828595 guibg=#adadad
hi GreyBack guifg=#e8e9ec guibg=#828595
"hi BlueGrey guifg=#757ca3 guibg=#adadad
"hi RedGrey guifg=#d9a67f guibg=#adadad
"hi GreenGrey guifg=#b6bd89 guibg=#adadad
hi User1 ctermfg=007 ctermbg=239 guibg=#4e4e4e guifg=#adadad
hi User1Inverse ctermfg=239 ctermbg=007 guibg=#adadad guifg=#4e4e4e 
hi NormalColor guifg=Black guibg=Green ctermbg=46 ctermfg=0
hi NormalColorInverse guifg=Green guibg=#adadad ctermbg=007 ctermfg=46
function! StatuslineGitBranch()
  let b:gitbranch=""
  if &modifiable
    try
      let l:dir=expand('%:p:h')
      let l:gitrevparse = system("git -C ".l:dir." rev-parse --abbrev-ref HEAD")
      if !v:shell_error
        let b:gitbranch="(".substitute(l:gitrevparse, '\n', '', 'g').") "
      endif
    catch
    endtry
  endif
endfunction

augroup GetGitBranch
  autocmd!
  autocmd VimEnter,WinEnter,BufEnter * call StatuslineGitBranch()
augroup END

