
set guifont=FiraMono\ Nerd\ Font\ Mono:h17
if exists("g:neovide")
    " Put anything you want to happen only in Neovide here
    let g:neovide_show_border = v:false
    let g:neovide_padding_top = 15
    let g:neovide_padding_bottom = 0
endif

