set undofile " Maintain undo history between sessions IMPORTAN IF FILE doesn't exist create it

set undodir=~/.config/nvim/undodir


set history=1000  " Keep commands written with :

set nocompatible " Make Vim more useful 

" set clipboard=unnamed " Use the OS clipboard by default (on versions compiled with `+clipboard`) 
set clipboard+=unnamed

set wildmenu " Enhance command-line completion
set wildmode=longest:list,full

set backspace=indent,eol,start " Allow backspace in insert mode

set ttyfast " Optimize for fast terminal connections 

set encoding=utf-8 nobomb  " Use UTF-8 without BOM

set keymap=greek_utf-8 " Add greek
inoremap <C-a> <C-^>
inoremap <C-α> <C-^>
inoremap <C-6> <C-^>

set iminsert=0 imsearch=0 " But not so default
" Change mapleader to space 
nnoremap <SPACE> <Nop>
let mapleader=" "
" Respect modeline in files
set modeline
set modelines=4
" Enable line numbers
set number
" Enable syntax highlighting
syntax on
set showmatch "Highlights matching parentheses"
set expandtab  " Transform tabs to spaces for everything except godot(mayde consider pyton also )
autocmd Filetype gdscript setlocal noexpandtab tabstop=4 shiftwidth=4
set shiftwidth=4      
set tabstop=4 " Make tabs as wide as four spaces

set hlsearch " Highlight searches 
set incsearch "Search while typing
set ignorecase " Ignore case of searches 
set smartcase "Unless you put some caps in your search term"


" Highlight dynamically as pattern is typed
set incsearch
" Always show status line
set laststatus=2
" Enable mouse in all modes
set mouse=a
" Disable error bells
set noerrorbells
" Don’t reset cursor to start of line when moving around.
set nostartofline
" Show the cursor position
set ruler
" Don’t show the intro message when starting Vim
set shortmess=atI
" Show the current mode I used to have that but know I have it on the
" statusline which is preferable so now I have it with no
set noshowmode
" Show the filename in the window titlebar
set title
" Show the (partial) command as it’s being typed
set showcmd
set signcolumn=number
" Use relative line numbers
set relativenumber
au BufReadPost * set relativenumber
" Start scrolling three lines before the horizontal window border
set scrolloff=3

set hidden " let me open many buffers without having to save them

" Strip trailing whitespace (,ss)
function! CrlfFunc()
		:%s/\r//g
endfunction

noremap <leader>ss :call CrlfFunc()<CR>

"Add matchit support
filetype plugin on
runtime macros/matchit.vim

"remove the weird tilda at eof
set fillchars+=eob:\ 

" Automatic commands
filetype on

autocmd FileType python setlocal textwidth=79 "PEP-8 made me do it

augroup filetype
  au! BufRead,BufNewFile *.mm set ft=objcpp
augroup END

augroup filetype
  au! BufRead,BufNewFile *.mm set ft=cpp
augroup END

augroup filetype
  au! BufRead,BufNewFile *.toolchain set ft=cmake
augroup END
"
"au BufNewFile,BufRead *.mm call dist#ft#FTmm()
"autocmd FileType javascript setlocal shiftwidth=2 softtabstop=2 expandtab

fun! Start()
    " Don't run if: we have commandline arguments, we don't have an empty
    " buffer, if we've not invoked as vim or gvim, or if we'e start in insert mode
    if argc() || line2byte('$') != -1 || v:progname !~? '^[-gmnq]\=vim\=x\=\%[\.exe]$' || &insertmode
        return
    endif

    " Start a new buffer ...
    enew

    " ... and set some options for it
    setlocal
        \ bufhidden=wipe
        \ buftype=nofile
        \ nobuflisted
        \ nocursorcolumn
        \ nocursorline
        \ nolist
        \ nonumber
        \ noswapfile
        \ norelativenumber
    for line in split(system('fortune '), '\n')
        call append('$', '        ' . l:line)
    endfor

    " No modifications to this buffer
    setlocal nomodifiable nomodified
    " When we go to insert mode start a new buffer, and start insert
    nnoremap <buffer><silent> e :enew<CR>
    nnoremap <buffer><silent> i :enew <bar> startinsert<CR>
    nnoremap <buffer><silent> o :enew <bar> startinsert<CR>
endfun
function! SearchNonSpaceWhitespace()
    let search_pattern = '\(\%u0009\|\%u000A\|\%u000B\|\%u000C\|\%u000D\|\%u0020\|\%u0085\|\%u00A0\|\%u1680\|\%u2000\|\%u2001\|\%u2002\|\%u2003\|\%u2004\|\%u2005\|\%u2006\|\%u2007\|\%u2008\|\%u2009\|\%u200A\|\%u2028\|\%u2029\|\%u202F\|\%u205F\|\%u3000\)'
    execute '/'.search_pattern
endfunction
" Run after "doing all the startup stuff"
" autocmd VimEnter * call Start()
source ~/.config/nvim/plugins.vimrc " Plugin related configs in a diffrent file 
source ~/.config/nvim/neovide_specific.vimrc
"source ~/.config/nvim/windows_specific.vimrc
source ~/.config/nvim/shortcuts.vimrc
source ~/.config/nvim/statusline.vimrc
"This one is for string eq
augroup highlight_yank
autocmd!
au TextYankPost * silent! lua vim.highlight.on_yank({higroup="Visual", timeout=200})
augroup END

" enable local .vimrc files
set exrc
set secure
