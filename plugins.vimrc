call plug#begin('~/.config/nvim/plugged')
"Default behaviours enhancment
Plug 'chrisbra/vim-diff-enhanced'
Plug 'google/vim-searchindex' "Show numbers bigger than 99
Plug 'tpope/vim-abolish'
Plug 'mbbill/undotree'
"Lsp/autocomplete
Plug 'neovim/nvim-lspconfig'
Plug 'barreiroleo/ltex_extra.nvim'
Plug 'folke/trouble.nvim' 
Plug 'ms-jpq/coq_nvim', {'branch': 'coq'}
Plug 'ms-jpq/coq.artifacts', {'branch': 'artifacts'}
"Treesiter
Plug 'nvim-treesitter/nvim-treesitter'
Plug 'sbdchd/neoformat'
"Theme
"Plug 'cocopon/iceberg.vim' "colorscheme
Plug 'tomasiser/vim-code-dark'
Plug 'adityastomar67/italicize'
"Plug 'rafamadriz/neon' "colorscheme
"Nvim unique
Plug 'kyazdani42/nvim-web-devicons'
Plug 'stevearc/oil.nvim'
Plug 'ibhagwan/fzf-lua'
Plug 'hedyhli/outline.nvim'
"Plug 'ms-jpq/coq.thirdparty' this was some spell related configs see commit 48dc1d3b
"Plug 'norcalli/nvim-colorizer.lua'
"Plug 'skywind3000/asyncrun.vim'
"Plug 'akinsho/toggleterm.nvim'
"Plug 'danymat/neogen' doxygen plugin, never used it but I would like to
"Plug 'mickael-menu/zk-nvim' As far as I am happy with logseq

call plug#end()

lua << EOF
require("oil").setup()

require("italicize").setup({
    italics = true,
    italics_groups = {
        "Comment",
    }
})

--vim.lsp.set_log_level("debug")
local lsp = require "lspconfig"
local coq = require "coq" 

local on_attach = function(client, bufnr)
    local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
    local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end
    local opts = { noremap=true, silent=true }
    -- hover instead of virtual text
    local signs = { Error = " ", Warn = " ", Hint = " ", Info = " " }
    for type, icon in pairs(signs) do
      local hl = "DiagnosticSign" .. type
      vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = hl })
    end
    vim.diagnostic.config {
        virtual_text = function()
            return {
    
                prefix = '',
                format = function(diagnostic)
                    return ""
                end,
            }
        end,
    }
    vim.keymap.set('n', '[d', vim.diagnostic.goto_prev)
    vim.keymap.set('n', ']d', vim.diagnostic.goto_next)
    vim.diagnostic.config({
      underline = false,
    })
    function spellcheck()
        vim.diagnostic.goto_next()
        require('fzf-lua').lsp_code_actions()
        --vim.lsp.buf.code_action({

        --    filter = function(action)
        --        return action.isPreferred
        --    end,
        --})
    end
    vim.o.updatetime = 250
    vim.cmd [[autocmd! CursorHold,CursorHoldI * lua vim.diagnostic.open_float(nil, {focus=false})]]
    buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')
    buf_set_keymap('n', 'gD', '<cmd>lua vim.lsp.buf.declaration()<CR>', opts)
    buf_set_keymap('n', 'gd', '<cmd>lua vim.lsp.buf.definition()<CR>', opts)
    buf_set_keymap('n', '<leader>dd', '<cmd>lua vim.diagnostic.disable()<CR>', opts)
    buf_set_keymap('n', '<leader>de', '<cmd>lua vim.diagnostic.enable()<CR>', opts)
    buf_set_keymap('n', '<space>rename', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
    buf_set_keymap('n', '<space>ca', '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)
    buf_set_keymap('n', '}}', '<cmd>lua spellcheck()<CR>', opts)


end



require'lspconfig'.ltex.setup(coq.lsp_ensure_capabilities( {
    cmd={ "/opt/homebrew/bin/ltex-ls"},
    on_attach = function(client, bufnr)
        on_attach(client,bufnr)
        require("ltex_extra").setup {
            load_langs = { "el-GR", "en-US" }, -- table <string> : languages for witch dictionaries will be loaded
            init_check = true, -- boolean : whether to load dictionaries on startup
            path = "$HOME/.local/share/nvim/.ltex", -- string : path to store dictionaries. Relative path uses current working directory
            }
    end,
    settings = {
		    ltex = {
		    	language = "el-GR",
		    },

	    },
    flags = {
        debounce_text_changes = 150,
        }
    }))

local servers = {'clangd','texlab','tsserver','html','rust_analyzer','gdscript','svelte','pyright'}--,'marksman','tailwindcss'}
for _, lsps in ipairs(servers) do
    lsp[lsps].setup(coq.lsp_ensure_capabilities( {
        on_attach = on_attach,
        flags = {
            debounce_text_changes = 150,
            }
        }))
end
vim.keymap.set('n', '<Leader>h', ':ClangdSwitchSourceHeader<CR>', { silent = true } )
-- Debug lsp example
--require('lspconfig')['clangd'].setup(
--    coq.lsp_ensure_capabilities({
--    cmd = { "clangd", "--log=verbose" },
--    on_attach = on_attach,
--    flags = {
--        debounce_text_changes = 150,
--        }
--    })
--)


--fzf lua
require('fzf-lua').setup{
    global_resume = true,
    --files = {
    --     path_shorten   = 6, 
    --},
    winopts ={
        hl ={
            cursorline     = 'Normal',    -- cursor line
        },
        preview = {
           --     title = false,
                layout = 'vertical',
                scrollbar      = 'float',         -- `false` or string:'float|border'
                scrolloff      = '-2',            -- float scrollbar offset from right
                scrollchars    = {'', '█' },      -- scrollbar chars ({ <full>, <empty> }
        }
    },
    keymap = {
        ["Esc"] = "abort"
    },
}
vim.keymap.set('n', '<leader>f', '<cmd>FzfLua files<cr>', { silent = true } )
vim.keymap.set('n', '<leader>s', '<cmd>FzfLua live_grep_native<cr>', { silent = true } )
vim.keymap.set('n', '<leader>b', '<cmd>FzfLua buffers<cr>', { silent = true } )
vim.keymap.set('n', '<leader>r', '<cmd>FzfLua resume<cr>', { silent = true } )
vim.keymap.set('n', '<leader>y', '<cmd>FzfLua registers<cr>', { silent = true } )

--Treesiter
local parser_config = require "nvim-treesitter.parsers".get_parser_configs()
parser_config.objc = {
  install_info = {
    url = "~/Documents/work/tree-sitter-objc2", -- local path or git repo
    files = {"src/parser.c"},
    -- optional entries:
    branch = "master", -- default branch in case of git repo if different from master
    generate_requires_npm = false, -- if stand-alone parser without npm dependencies
  },
  filetype = "objcpp", -- if filetype does not match the parser name
}

require'nvim-treesitter.configs'.setup{
    highlight = {
        enable = true,              -- false will disable the whole extension
 -- disable = { "objc"}, -- list of language that will be disabled
        additional_vim_regex_highlighting = false,
    },
}

require("outline").setup()
vim.keymap.set( 'n', '<Leader>v', ':Outline<CR>', {silent=true})

EOF


if &diff
    let &diffexpr='EnhancedDiff#Diff("git diff", "--diff-algorithm=patience")'
endif
"
"COQ related
COQnow -s
"" Syntax highlight
"" Enable highlighting of C++11 attributes
"let g:cpp_attributes_highlight = 1
"" Highlight struct/class member variables (affects both C and C++ files)
"let g:cpp_member_highlight = 1

" theme and other settings that get reseted if set before theme
set background=dark " Use the Solarized Dark theme    
set termguicolors 
"colorscheme iceberg
colorscheme codedark
hi Normal guibg=NONE ctermbg=NONE
highlight Normal guibg=none
highlight NonText guibg=none
set cursorline
"hi CursorLine term=underline cterm=underline gui=underline guibg=none

