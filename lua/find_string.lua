local M = {}

local parsers = require("nvim-treesitter.parsers")
local ts_utils = require("nvim-treesitter.ts_utils")

-- Function to extract string literals from C++ code
function M.extractStringLiterals()
    local bufnr = vim.api.nvim_get_current_buf()
    local parser = parsers.get_parser(bufnr, "cpp")

    if not parser then
        print("Parser not found for the current buffer.")
        return
    end

    local root = parser:parse():root()

    local literals = {}
    local function traverse(node)
        if node:type() == "string_literal" then
            table.insert(literals, node:source())
        end

        for _, child in ipairs(node:children()) do
            traverse(child)
        end
    end

    traverse(root)

    return literals
end

return M

